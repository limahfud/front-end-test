
angular.module('binarApp',[])
	.controller('MainController', ['$scope', function($scope) {
		var premiumPrice = 7000, pertalitePrice = 8000, pertamaxPrice = 9000;
				
		$scope.isTransaction = false;

		$scope.transactions = [
			{
				type : 'premium',
				liters : 2,
				timestamp : new Date()
			},
			{
				type : 'pertamax',
				liters : 1.6,
				timestamp : new Date()
			},
			{
				type : 'pertalite',
				liters : 5,
				timestamp : new Date()
			},
			{
				type : 'pertalite',
				liters : 1,
				timestamp : new Date()
			},
			{
				type : 'premium',
				liters : 40,
				timestamp : new Date()
			}
		];

		$scope.getPrice = function(transaction) {
			if (transaction.type == 'premium') return premiumPrice;
			if (transaction.type == 'pertalite') return pertalitePrice;
			return pertamaxPrice;
		}

		$scope.addTransaction = function(data) {
			$scope.data.push(data);
		};

		$scope.editTransaction = function(index, data) {
			$scope.data[index] = data;
		};

		



	}]);